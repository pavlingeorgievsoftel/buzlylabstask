package com.example.searchpoststwitter.models;

import java.io.Serializable;
import java.util.List;

public class ListPhotos implements Serializable {
    public int page;
    public int pages;
    public int perpage;
    public int total;
    public List<Photo> photo;
}
