package com.example.searchpoststwitter.models;

import java.io.Serializable;

public class Player implements Serializable {
    public String name;
    public float wins=0;
    public float losses=0;

    public Player(String name){
        this.name = name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void updateWins(){
        this.wins+=1;
    }

    public void updateLosses(){
        this.losses+=1;
    }

    public float getCoef() {
        float matches = wins + losses;
        return wins/matches;
    }
}
