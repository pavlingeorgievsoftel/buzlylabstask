package com.example.searchpoststwitter.models;

import java.io.Serializable;

public class Photo implements Serializable {
    public String id;
    public String owner;
    public String secret;
    public String server;
    public String title;
}
