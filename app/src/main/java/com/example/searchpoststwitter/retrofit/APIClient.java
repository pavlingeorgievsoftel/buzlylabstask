package com.example.searchpoststwitter.retrofit;

import androidx.annotation.NonNull;
import com.example.searchpoststwitter.Utils;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static Retrofit retrofitTwitter = null;
    private static Retrofit retrofitFlickr = null;

    public static Retrofit getTwitterClient() {
        if (retrofitTwitter == null) {
            OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
            builder.addInterceptor(new ConnectivityInterceptor());
            OkHttpClient client = builder.build();

            retrofitTwitter = new Retrofit.Builder()
                    .baseUrl(Utils.twitterApiUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofitTwitter;
    }

    public static Retrofit getFlickrClient() {
        if (retrofitFlickr == null) {
            OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
            builder.addInterceptor(new ConnectivityInterceptor());
            OkHttpClient client = builder.build();

            retrofitFlickr = new Retrofit.Builder()
                    .baseUrl(Utils.flickrApiUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofitFlickr;
    }

    static class ConnectivityInterceptor implements Interceptor {
        @NonNull
        @Override
        public Response intercept(@NonNull Chain chain) throws IOException {
            Request.Builder builder = chain.request()
                    .newBuilder()
                    .addHeader("Authorization", "Bearer " + Utils.twitterToken);

            return chain.proceed(builder.build());
        }
    }
}
