package com.example.searchpoststwitter.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {
    @GET("/1.1/search/tweets.json")
    Call<ResponseBody> searchTweets(@Query("q") String keyword);

    @FormUrlEncoded
    @POST("/services/rest")
    Call<ResponseBody> getRecentPhotos(@Field("api_key") String api_key,
                                       @Field("method") String method,
                                       @Field("format") String format,
                                       @Field("nojsoncallback") int nojsoncallback);
}
