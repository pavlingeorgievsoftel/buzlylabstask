package com.example.searchpoststwitter.viewModels;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.searchpoststwitter.retrofit.APIClient;
import com.example.searchpoststwitter.retrofit.APIInterface;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchTwitterActivityViewModel extends ViewModel {
    private APIInterface apiInterface;
    public MutableLiveData<ResponseBody> mSearchTweets;

    public LiveData<ResponseBody> searchTweetsObserver(String keyWord) {
        if (mSearchTweets == null) {
            mSearchTweets = new MutableLiveData<>();

            searchTweets(keyWord);
        }

        return mSearchTweets;
    }

    public void searchTweets(String keyWord) {
        apiInterface = APIClient.getTwitterClient().create(APIInterface.class);
        final Call<ResponseBody> checkPassCode = apiInterface.searchTweets(keyWord);

        checkPassCode.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call,@NonNull Response<ResponseBody> response) {
                Log.d("checkresponse", "searchTweetsResponse - " + response);

                if (mSearchTweets != null && response.code() == 200)
                    mSearchTweets.setValue(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call,@NonNull Throwable t) {
                Log.d("checkexception", "searchTweetsException - " + t.getMessage());
            }
        });
    }
}
