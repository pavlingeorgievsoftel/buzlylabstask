package com.example.searchpoststwitter.viewModels;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.searchpoststwitter.Utils;
import com.example.searchpoststwitter.retrofit.APIClient;
import com.example.searchpoststwitter.retrofit.APIInterface;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BrowsePhotosActivityViewModel extends ViewModel {
    private APIInterface apiInterface;
    public MutableLiveData<ResponseBody> mRecentPhotos;

    public LiveData<ResponseBody> getRecentPhotosObserver() {
        if (mRecentPhotos == null) {
            mRecentPhotos = new MutableLiveData<>();

            getRecentPhotos();
        }

        return mRecentPhotos;
    }

    public void getRecentPhotos() {
        apiInterface = APIClient.getFlickrClient().create(APIInterface.class);
        final Call<ResponseBody> checkPassCode = apiInterface.getRecentPhotos(Utils.flickrApiKey,"flickr.photos.getRecent","json",1);

        checkPassCode.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.d("checkresponse", "getRecentPhotosResponse - " + response);

                if (mRecentPhotos != null && response.code() == 200)
                    mRecentPhotos.setValue(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call,@NonNull Throwable t) {
                Log.d("checkexception", "getRecentPhotosException - " + t.getMessage());
            }
        });
    }
}
