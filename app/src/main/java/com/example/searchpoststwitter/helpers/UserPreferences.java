package com.example.searchpoststwitter.helpers;


import android.content.Context;
import android.content.SharedPreferences;
import com.example.searchpoststwitter.BuzlyLabsApplication;

/**
 * Created by user on 2/8/17.
 */

public class UserPreferences {

    private static final String PREFS_NAME = "settings";

    public static SharedPreferences getInstance() {
        return BuzlyLabsApplication.getContext().getSharedPreferences("com.example.searchpoststwitter", Context.MODE_PRIVATE);
    }

    public static Boolean get(String var, Boolean defaultValue) {
        return getInstance().getBoolean(var, defaultValue);
    }

    public static void set(String key, Boolean value) {
        getInstance().edit().putBoolean(key, value).apply();
    }

    public static Integer get(String var, Integer defaultValue) {
        return getInstance().getInt(var, defaultValue);
    }

    public static void set(String key, Integer value) {
        getInstance().edit().putInt(key, value).apply();
    }

    public static String get(String var, String defaultValue) {
        return getInstance().getString(var, defaultValue);
    }

    public static void set(String key, String value) {
        getInstance().edit().putString(key, value).apply();
    }

    public static Long get(String var, Long defaultValue) {
        return getInstance().getLong(var, defaultValue);
    }

    public static void set(String key, Long value) {
        getInstance().edit().putLong(key, value).apply();
    }

    public static void clear() {
        getInstance().edit().clear().apply();
    }
}
