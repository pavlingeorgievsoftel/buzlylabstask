package com.example.searchpoststwitter.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;

import com.example.searchpoststwitter.R;
import com.example.searchpoststwitter.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        binding.btnSearchTwitter.setOnClickListener(v -> startActivity(new Intent(MainActivity.this,SearchTwitterActivity.class)));
        binding.btnBrowsePhotos.setOnClickListener(v -> startActivity(new Intent(MainActivity.this,BrowsePhotosActivity.class)));
        binding.btnFootball.setOnClickListener(v -> startActivity(new Intent(MainActivity.this,FootballRanking.class)));
    }
}