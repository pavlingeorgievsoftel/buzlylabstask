package com.example.searchpoststwitter.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.example.searchpoststwitter.R;
import com.example.searchpoststwitter.adapters.FootballRankingAdapter;
import com.example.searchpoststwitter.databinding.ActivityFootballRankingBinding;
import com.example.searchpoststwitter.helpers.UserPreferences;
import com.example.searchpoststwitter.models.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FootballRanking extends AppCompatActivity {
    private ActivityFootballRankingBinding binding;

    private List<Player> listPlayers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_football_ranking);

        getResults();
        setEditTextListeners();

        binding.btnSave.setOnClickListener(v -> updateResults());
    }

    void processResults() {
        String results = UserPreferences.get("results", "Amos,4,Diego,5;Amos,1,Diego,5;Amos,2,Diego,5;Amos,0,Diego,5;Amos,6,Diego,5;Amos,5,Diego,2;Amos,4,Diego,0;Joel,4,Diego,5;Tim,4,Amos,5;Tim,5,Amos,2;Amos,3,Tim,5;Amos,5,Tim,3;Amos,5,Joel,4;Joel,5,Tim,2");

        String[] listMatches = results.split(";");

        for (String match : listMatches) {
            List<String> matchResult = Arrays.asList(match.split(","));

            String player1 = matchResult.get(0);
            int result1 = Integer.parseInt(matchResult.get(1));

            String player2 = matchResult.get(2);
            int result2 = Integer.parseInt(matchResult.get(3));

            if (result1 > result2) {       // check match winner and update wins list
                Player playerObject1 = findPlayerByName(player1);
                playerObject1.updateWins();

                Player playerObject2 = findPlayerByName(player2);
                playerObject2.updateLosses();
            } else if (result1 < result2) {
                Player playerObject2 = findPlayerByName(player2);
                playerObject2.updateWins();

                Player playerObject1 = findPlayerByName(player1);
                playerObject1.updateLosses();
            }
        }

        Collections.sort(listPlayers, (lhs, rhs) -> Float.compare(rhs.getCoef(), lhs.getCoef()));
    }

    public Player findPlayerByName(String name) {
        Player playerObject = listPlayers.stream().filter(player -> name.equals(player.getName())).findFirst().orElse(null);
        if (playerObject == null) {
            playerObject = new Player(name);
            listPlayers.add(playerObject);
        }

        return playerObject;
    }

    void initRecyclerView() {
        binding.rvRanking.setAdapter(new FootballRankingAdapter(getApplicationContext(), listPlayers));
        binding.rvRanking.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    private void setEditTextListeners() {
        setEditTextListener(binding.etPLayer1);
        setEditTextListener(binding.etPLayer2);
        setEditTextListener(binding.etPlayer1Score);
        setEditTextListener(binding.etPlayer2Score);
    }

    private void setEditTextListener(EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (binding.etPLayer1.getText().length() > 0 && binding.etPLayer2.getText().length() > 0 && binding.etPlayer1Score.getText().length() > 0 && binding.etPlayer2Score.getText().length() > 0)
                    enableButton();
                else
                    disableButton();
            }
        });
    }

    void enableButton() {
        binding.btnSave.setEnabled(true);
        binding.btnSave.setAlpha(1f);
    }

    void disableButton() {
        binding.btnSave.setEnabled(false);
        binding.btnSave.setAlpha(0.5f);
    }

    private void updateResults() {
        StringBuilder results = new StringBuilder();
        String resultsSharedPref = UserPreferences.get("results", "Amos,4,Diego,5;Amos,1,Diego,5;Amos,2,Diego,5;Amos,0,Diego,5;Amos,6,Diego,5;Amos,5,Diego,2;Amos,4,Diego,0;Joel,4,Diego,5;Tim,4,Amos,5;Tim,5,Amos,2;Amos,3,Tim,5;Amos,5,Tim,3;Amos,5,Joel,4;Joel,5,Tim,2");

        results.append(resultsSharedPref).append(";").append(binding.etPLayer1.getText()).append(",").append(binding.etPlayer1Score.getText()).append(",").append(binding.etPLayer2.getText()).append(",").append(binding.etPlayer2Score.getText());

        UserPreferences.set("results", results.toString());

        getResults();
    }

    void getResults() {
        listPlayers.clear();
        processResults();
        initRecyclerView();
    }
}