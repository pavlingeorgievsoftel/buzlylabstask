package com.example.searchpoststwitter.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.example.searchpoststwitter.R;
import com.example.searchpoststwitter.adapters.PhotosAdapter;
import com.example.searchpoststwitter.databinding.ActivityBrowsePhotosBinding;
import com.example.searchpoststwitter.models.Photo;
import com.example.searchpoststwitter.models.Photos;
import com.example.searchpoststwitter.viewModels.BrowsePhotosActivityViewModel;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;

public class BrowsePhotosActivity extends AppCompatActivity {
    private ActivityBrowsePhotosBinding binding;
    private int targetPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_browse_photos);

        getRecentPhotos();
    }

    void getRecentPhotos() {
        binding.progressBar.setVisibility(View.VISIBLE);
        BrowsePhotosActivityViewModel model = ViewModelProviders.of(BrowsePhotosActivity.this).get(BrowsePhotosActivityViewModel.class);

        if (model.mRecentPhotos != null && model.mRecentPhotos.hasObservers())
            model.getRecentPhotos();
        else
            model.getRecentPhotosObserver()
                    .observe(BrowsePhotosActivity.this, response -> {
                        Log.d("checkinfo", "getRecentPhotos");

                        try {
                            Photos photos = new Gson().fromJson(response.string(), Photos.class);
                            initRecyclerView(photos.photos.photo);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
//
                        binding.progressBar.setVisibility(View.GONE);
                    });
    }

    private void initRecyclerView(List<Photo> listPhotos) {
        binding.rvPhotos.setAdapter(new PhotosAdapter(getApplicationContext(), listPhotos));

        LinearLayoutManager linearLayout = new LinearLayoutManager(getApplicationContext());
        linearLayout.setOrientation(RecyclerView.HORIZONTAL);

        binding.rvPhotos.setLayoutManager(linearLayout);
        addSnapHelper();
    }

    private void addSnapHelper() {
        PagerSnapHelper snapHelper = new PagerSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                View centerView = findSnapView(layoutManager);
                if (centerView == null)
                    return RecyclerView.NO_POSITION;

                int position = layoutManager.getPosition(centerView);
                targetPosition = -1;
                if (layoutManager.canScrollHorizontally()) {
                    if (velocityX < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }

                if (layoutManager.canScrollVertically()) {
                    if (velocityY < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }

                final int firstItem = 0;
                final int lastItem = layoutManager.getItemCount() - 1;
                targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                return targetPosition;
            }
        };

        binding.rvPhotos.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(binding.rvPhotos);
    }
}