package com.example.searchpoststwitter.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.searchpoststwitter.R;
import com.example.searchpoststwitter.adapters.TwitsAdapter;
import com.example.searchpoststwitter.databinding.ActivitySearchTwitterBinding;
import com.example.searchpoststwitter.models.ListTwits;
import com.example.searchpoststwitter.models.Twit;
import com.example.searchpoststwitter.viewModels.SearchTwitterActivityViewModel;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

public class SearchTwitterActivity extends AppCompatActivity {
    private ActivitySearchTwitterBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_twitter);

        setEditTextListener(binding.etSearch);
        binding.btnSearch.setOnClickListener(v -> searchTweets(binding.etSearch.getText().toString()));
    }

    void searchTweets(String keyword) {
        binding.progressBar.setVisibility(View.VISIBLE);
        SearchTwitterActivityViewModel model = ViewModelProviders.of(SearchTwitterActivity.this).get(SearchTwitterActivityViewModel.class);

        if (model.mSearchTweets != null && model.mSearchTweets.hasObservers())
            model.searchTweets(keyword);
        else
            model.searchTweetsObserver(keyword)
                    .observe(SearchTwitterActivity.this, response -> {
                        Log.d("checkinfo", "onChanged_searchTweets");

                        try {
                            ListTwits listTwits = new Gson().fromJson(response.string(), ListTwits.class);
                            initRecyclerView(listTwits.statuses);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        binding.progressBar.setVisibility(View.GONE);
                    });
    }

    public void setEditTextListener(EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (binding.etSearch.getText().length() > 1)
                    enableButton();
                else
                    disableButton();
            }
        });
    }

    void enableButton() {
        binding.btnSearch.setEnabled(true);
        binding.btnSearch.setAlpha(1f);
    }

    void disableButton() {
        binding.btnSearch.setEnabled(false);
        binding.btnSearch.setAlpha(0.5f);
    }

    void initRecyclerView(List<Twit> listTwits) {
        binding.rvTwits.setAdapter(new TwitsAdapter(getApplicationContext(), listTwits));
        binding.rvTwits.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }
}