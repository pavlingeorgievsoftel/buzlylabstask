package com.example.searchpoststwitter;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

public class BuzlyLabsApplication extends Application implements Application.ActivityLifecycleCallbacks {
    private static BuzlyLabsApplication instance;

    public static BuzlyLabsApplication getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();

        registerActivityLifecycleCallbacks(BuzlyLabsApplication.this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
