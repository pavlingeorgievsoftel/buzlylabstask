package com.example.searchpoststwitter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Utils {
    public static String flickrApiUrl = "https://api.flickr.com";
    public static String flickrApiKey = "16168949c339271a7c5c4136a2bc31f6";

    public static String twitterApiUrl = "https://api.twitter.com";
    public static String twitterToken = "AAAAAAAAAAAAAAAAAAAAAE5pKAEAAAAAxhlTK6RI%2Byv6RBssDLsg%2BIYj12c%3DrINTnWtkYjTFjcCresx4QNPVGFLQobMTV24FIDQHTRe5m6gxJa"; // it's not secure

    public static String photoBaseUrl = "https://live.staticflickr.com";

    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();

        return (netInfo != null && netInfo.isConnected());
    }

    public static boolean isInternetAvailable() {
        try {
            URL url = new URL("https://www.google.com/");
            HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
            urlc.setRequestProperty("User-Agent", "test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(3000);
            urlc.connect();

            if (urlc.getResponseCode() == 200) {
                urlc.getInputStream().close();
                return true;
            } else {
                urlc.getErrorStream().close();
                return false;
            }
        } catch (IOException e) {
            return false;
        }
    }
}
