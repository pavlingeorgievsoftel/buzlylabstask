package com.example.searchpoststwitter.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.searchpoststwitter.R;
import com.example.searchpoststwitter.Utils;
import com.example.searchpoststwitter.models.Photo;

import java.util.List;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosViewHolder> {
    private Context mContext;
    private List<Photo> mListPhotos;

    public PhotosAdapter(Context context, List<Photo> listPhotos) {
        mContext = context;
        mListPhotos = listPhotos;
    }

    @Override
    public PhotosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_photo, parent, false);

        return new PhotosViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PhotosViewHolder holder, int position) {
        String photoUrl = Utils.photoBaseUrl + "/" + mListPhotos.get(position).server + "/" + mListPhotos.get(position).id + "_" + mListPhotos.get(position).secret + ".jpg";

        Log.d("checkimage","photoUrl - " + photoUrl);

        Glide
                .with(mContext)
                .load(photoUrl)
                .centerCrop()
                .into(holder.ivPhoto);

        holder.tvTitle.setText(mListPhotos.get(position).title);
    }

    @Override
    public int getItemCount() {
        return mListPhotos.size();
    }
}

class PhotosViewHolder extends RecyclerView.ViewHolder {
    public ImageView ivPhoto;
    public TextView tvTitle;

    public PhotosViewHolder(View v) {
        super(v);

        ivPhoto = v.findViewById(R.id.ivPhoto);
        tvTitle = v.findViewById(R.id.tvTitle);
    }
}
