package com.example.searchpoststwitter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.searchpoststwitter.R;
import com.example.searchpoststwitter.models.Player;

import java.util.List;

public class FootballRankingAdapter extends RecyclerView.Adapter<FootballRankingViewHolder> {
    private Context mContext;
    private List<Player> mListPlayers;

    public FootballRankingAdapter(Context context, List<Player> listPlayers) {
        mContext = context;
        mListPlayers = listPlayers;
    }

    @Override
    public FootballRankingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rank, parent, false);

        return new FootballRankingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FootballRankingViewHolder holder, int position) {
        holder.tvRank.setText(String.valueOf(position+1));
        holder.tvName.setText(mListPlayers.get(position).name);
        holder.tvWins.setText(String.valueOf((int)mListPlayers.get(position).wins));
        holder.tvLosses.setText(String.valueOf((int)mListPlayers.get(position).losses));
    }

    @Override
    public int getItemCount() {
        return mListPlayers.size();
    }
}

class FootballRankingViewHolder extends RecyclerView.ViewHolder {
    public TextView tvRank;
    public TextView tvName;
    public TextView tvWins;
    public TextView tvLosses;

    public FootballRankingViewHolder(View v) {
        super(v);

        tvRank = v.findViewById(R.id.tvRank);
        tvName = v.findViewById(R.id.tvName);
        tvWins = v.findViewById(R.id.tvWins);
        tvLosses = v.findViewById(R.id.tvLosses);
    }
}
