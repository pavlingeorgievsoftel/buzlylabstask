package com.example.searchpoststwitter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.searchpoststwitter.R;
import com.example.searchpoststwitter.models.Twit;

import java.util.List;

/**
 * Created by p.georgiev on 25/01/2017.
 */

public class TwitsAdapter extends RecyclerView.Adapter<TwitsViewHolder> {
    private List<Twit> mListTwits;

    public TwitsAdapter(Context context,List<Twit> listTwits) {
        mListTwits = listTwits;
    }

    @Override
    public TwitsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_twit, parent, false);

        return new TwitsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final TwitsViewHolder holder, int position) {
        holder.tvText.setText(mListTwits.get(position).text);
    }

    @Override
    public int getItemCount() {
        return mListTwits.size();
    }
}

class TwitsViewHolder extends RecyclerView.ViewHolder {
    public TextView tvText;

    public TwitsViewHolder(View v) {
        super(v);
        tvText = v.findViewById(R.id.tvText);
    }
}
